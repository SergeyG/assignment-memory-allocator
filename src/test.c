#include "test.h"
#include "print_color.h"


struct test_result success_test_result = {
        .success = true,
        .message = "OK"
};


#define TEST_ENV_HEAP_SIZE 10000

void *heap;
struct block_header *first_block;

bool prepare_test_env() {
    heap = heap_init(TEST_ENV_HEAP_SIZE);
    first_block = (struct block_header *) heap;
    if (heap == NULL || first_block == NULL) {
        return false;
    }

    return true;
}

void reset_heap_size() {
    first_block->capacity.bytes = TEST_ENV_HEAP_SIZE;
}

static size_t count_allocated_blocks_num(struct block_header *start_block) {
    size_t num = 0;
    struct block_header *next_block = start_block;
    while (next_block->next != NULL) {
        num++;
        next_block = next_block->next;
    }

    return num;
}

static size_t count_freed_blocks_num(struct block_header *start_block) {
    size_t num = 0;
    struct block_header *block = start_block;
    while (block != NULL) {
        if (block->is_free) {
            num++;
        }
        block = block->next;
    }

    return num;
}

#define TEST_MALLOC_SIZE 1000

struct test_result test_malloc() {
    debug_heap(stdout, first_block);
    void *mem = _malloc(TEST_MALLOC_SIZE);
    debug_heap(stdout, first_block);
    if (mem == NULL || first_block->capacity.bytes != TEST_MALLOC_SIZE) {
        return (struct test_result) {
                .success = false,
                .message = "Memory not allocated"
        };
    }
    _free(mem);

//    debug_heap(stdout, first_block);

    return success_test_result;
}

#define TEST_FREE_ONE_BLOCK_FIRST_SIZE 2000
#define TEST_FREE_ONE_BLOCK_SECOND_SIZE 3000

struct test_result test_free_one_block() {
    debug_heap(stdout, first_block);
    void *mem[] = {
            _malloc(TEST_FREE_ONE_BLOCK_FIRST_SIZE),
            _malloc(TEST_FREE_ONE_BLOCK_SECOND_SIZE)
    };
    debug_heap(stdout, first_block);

    if (count_allocated_blocks_num(first_block) != 2) {
        for (size_t i = 0; i < 2; _free(mem[i++]));

        return (struct test_result) {
                .success = false,
                .message = "Incorrect number of allocated blocks"
        };
    }

    _free(mem[0]);
    debug_heap(stdout, first_block);
    if (count_freed_blocks_num(first_block) != 2) {
        return (struct test_result) {
                .success = false,
                .message = "Incorrect number of freed blocks"
        };
    }

    for (int i = 1; i >= 0; _free(mem[i--]));

//    debug_heap(stdout, first_block);

    return success_test_result;
}

#define TEST_FREE_TWO_BLOCKS_FIRST_SIZE 1000
#define TEST_FREE_TWO_BLOCKS_SECOND_SIZE 2000
#define TEST_FREE_TWO_BLOCKS_THIRD_SIZE 3000

struct test_result test_free_two_blocks() {
    debug_heap(stdout, first_block);
    void *mem[] = {
            _malloc(TEST_FREE_TWO_BLOCKS_FIRST_SIZE),
            _malloc(TEST_FREE_TWO_BLOCKS_SECOND_SIZE),
            _malloc(TEST_FREE_TWO_BLOCKS_THIRD_SIZE)
    };
    debug_heap(stdout, first_block);

    if (count_allocated_blocks_num(first_block) != 3) {
        for (size_t i = 0; i < 3; _free(mem[i++]));

        return (struct test_result) {
                .success = false,
                .message = "Incorrect number of allocated blocks"
        };
    }

    _free(mem[0]);
    _free(mem[1]);
    debug_heap(stdout, first_block);

    if (count_freed_blocks_num(first_block) != 3) {
        return (struct test_result) {
                .success = false,
                .message = "Incorrect number of freed blocks"
        };
    }

    for (int i = 2; i >= 0; _free(mem[i--]));

//    debug_heap(stdout, first_block);

    return success_test_result;
}

#define TEST_GROW_HEAP_EXTENDING_FIRST_SIZE 9000
#define TEST_GROW_HEAP_EXTENDING_SECOND_SIZE 2000

struct test_result test_grow_heap_extending() {
    debug_heap(stdout, first_block);
    void *mem[] = {
            _malloc(TEST_GROW_HEAP_EXTENDING_FIRST_SIZE),
            _malloc(TEST_GROW_HEAP_EXTENDING_SECOND_SIZE)
    };
    debug_heap(stdout, first_block);

    if (count_allocated_blocks_num(first_block) != 2) {
        return (struct test_result) {
                .success = false,
                .message = "Heap not grown"
        };
    }
    for (int i = 1; i >= 0; _free(mem[i--]));

//    debug_heap(stdout, first_block);

    return success_test_result;
}

static inline void mmap_region(size_t length, void *addr) {
    size_t count = (size_t) addr / getpagesize();
    size_t remains = (size_t) addr % getpagesize();

    uint8_t *total = (uint8_t *) (getpagesize() * (count + (remains > 1)));

    mmap(total, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
}

static inline struct block_header* get_block_by_allocated_data(void* data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}

#define TEST_GROW_HEAP_NOT_EXTENDING_LENGTH 1000
#define TEST_GROW_HEAP_NOT_EXTENDING_ALLOCATE 100000

struct test_result test_grow_heap_not_extending() {
    reset_heap_size();
    debug_heap(stdout, first_block);

    struct block_header *addr = first_block;
    while (addr->next != NULL)
        addr = addr->next;

    mmap_region(TEST_GROW_HEAP_NOT_EXTENDING_LENGTH, addr);

    void *allocated = _malloc(TEST_GROW_HEAP_NOT_EXTENDING_ALLOCATE);

    if (addr == get_block_by_allocated_data(allocated)) {
        return (struct test_result) {
                .success = false,
                .message = "New region allocated in the old region"
        };
    }

    debug_heap(stdout, first_block);

    _free(allocated);

//    debug_heap(stdout, first_block);

    return success_test_result;
}

struct test tests[] = {
        {
                "Normal memory allocation",
                test_malloc
        },
        {
                "Freeing one block from several allocated ones",
                test_free_one_block
        },
        {
                "Freeing two blocks from several allocated ones",
                test_free_two_blocks
        },
        {
                "The memory has run out, the new memory region expands the old one",
                test_grow_heap_extending
        },
        {
                "The memory has run out, the old memory region cannot be expanded due to a different allocated range of addresses, the new region is allocated in a different place",
                test_grow_heap_not_extending
        }
};

enum {
    NUM_TESTS = sizeof(tests) / sizeof(tests[0])
};

void test_all() {
    if (!prepare_test_env()) {
        printf("Couldn't prepare testing environment");
        exit(EXIT_FAILURE);
    }

    size_t passed = 0;

    for (size_t i = 0; i < NUM_TESTS; i++) {
        printf("%zu. %s\n" COLOR_BLUE, i + 1, tests[i].name);
        struct test_result result = (*tests[i].test_function)();
        printf(COLOR_RESET);
        if (result.success) {
            printf(COLOR_GREEN "[ V ] %s\n" COLOR_RESET, result.message);
            passed++;
        } else {
            printf(COLOR_RED "[ X ] %s\n" COLOR_RESET, result.message);
        }
    }

    if (passed == NUM_TESTS) {
        printf(COLOR_GREEN "\nPASSED TESTS: %zu/%d" COLOR_RESET "\n", passed, NUM_TESTS);
        exit(EXIT_SUCCESS);
    } else {
        printf(COLOR_RED "\nPASSED TESTS: %zu/%d" COLOR_RESET "\n", passed, NUM_TESTS);
        exit(EXIT_FAILURE);
    }
}
